/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pop-window-helper.h"
Q_GLOBAL_STATIC(PopWindowHelper, s_popWindowHelper)

PopWindowHelper *PopWindowHelper::self()
{
    return s_popWindowHelper;
}

PopWindowHelper::PopWindowHelper(QObject *parent) : QObject(parent)
{

}

PopWindowHelper::~PopWindowHelper()
{
    freeWindow();
}

void PopWindowHelper::initWindow()
{
    if (!m_popWindow) {
        m_popWindow = new DeviceWindow();
    }
    if (!m_sliderWindow) {
        m_sliderWindow = new VolumeWindow();
    }
}

void PopWindowHelper::freeWindow()
{
    if (m_popWindow) {
        m_popWindow->deleteLater();
        m_popWindow = nullptr;
    }
    if (m_sliderWindow) {
        m_sliderWindow->deleteLater();
        m_sliderWindow = nullptr;
    }
}

void PopWindowHelper::showWidget(const QString& icon) const
{
    if (m_popWindow) {
        m_popWindow->setAction(icon);
        m_popWindow->dialogShow();
    }
}

void PopWindowHelper::showWidget(uint value) const
{
    if (m_sliderWindow) {
        m_sliderWindow->setBrightValue(value);
        m_sliderWindow->dialogBrightShow();
    }
}

void PopWindowHelper::showWidget(uint value, bool mute) const
{
    if (m_sliderWindow) {
        m_sliderWindow->setVolumeMuted(mute);
        m_sliderWindow->setVolumeLevel(value);
        m_sliderWindow->dialogVolumeShow();
    }
}

int PopWindowHelper::getMaxVolume()
{
    if (m_sliderWindow) {
        return m_sliderWindow->getVolumeMax();
    }
}
