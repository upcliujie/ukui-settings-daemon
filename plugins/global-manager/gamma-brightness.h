#ifndef GAMMABRIGHTNESS_H
#define GAMMABRIGHTNESS_H


#include <QDBusReply>
#include <QDBusMessage>
#include <QDBusArgument>
#include <QDBusInterface>

#include "clib-syslog.h"
#include "usd_global_define.h"
#include "abstract-brightness.h"

class GammaBrightness : public AbstractBrightness
{
    Q_OBJECT
public:
    GammaBrightness(QObject *parent);

    /**
     * @brief getBrightness
     * @return
     */
    int getBrightness();

    /**
     * @brief setBrightness
     * @param brightness
     * @return
     */
    int setBrightness(int brightness);

    /**
     * @brief connectTheSignal
     * @return
     */
    int connectTheSignal();

    /**
     * @brief isEnable
     * @return
     */
     QString backend();

private:
    QDBusInterface *m_pGmDbusInterface = nullptr;
};

#endif // GAMMABRIGHTNESS_H
