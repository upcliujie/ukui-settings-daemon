/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMMAMANAGERWAYLAND_H
#define GAMMAMANAGERWAYLAND_H

#include <QNetworkAccessManager>
#include <QtDBus/QDBusMetaType>
#include <QNetworkReply>
#include <QDBusInterface>
#include <QStandardPaths>
#include <QDBusArgument>
#include <QtAlgorithms>
#include <QtGlobal>
#include <QSettings>
#include <QObject>
#include <QTimer>

#include <math.h>

#include "gamma-manager-location.h"
#include "clock-skew-notifier.h"
#include "gamma-color-info.h"
#include "plugin-manager-interface.h"
#include "clib-syslog.h"
#include "gamma-manager-gtkconfig.h"
#include "gamma-manager-define.h"
#include "QGSettings/qgsettings.h"

class GammaManagerWayland: public ManagerInterface
{
    Q_OBJECT
public:
    GammaManagerWayland();
    GammaManagerWayland(GammaManagerWayland&)=delete;
    GammaManagerWayland&operator=(const GammaManagerWayland&)=delete;
public:
    ~GammaManagerWayland();
    static GammaManagerWayland *GammaManagerWaylandNew();

    virtual bool Start();
    virtual void Stop();
public Q_SLOTS:

    /**
     * @brief doQtSettingsChanged
     * @param setKey
     */
    void doQtSettingsChanged(QString setKey);

    /**
     * @brief doColorSettingsChanged
     * @param setKey
     */
    void doColorSettingsChanged(QString setKey);

    /**
     * @brief doCheckTimeout
     */
    void doCheckTimeout();
private:

    /**
     * @brief syncColorSetToKwin
     */
    void syncColorSetToKwin();

    /**
     * @brief syncColorToDbus
     */
    void syncColorToDbus(QDBusInterface &colorIft, QHash<QString, QVariant> nightConfig);

    /**
     * @brief syncEyeCareToDbus
     */
    void syncEyeCareToDbus(QDBusInterface &colorIft, QHash<QString, QVariant> nightConfig);

    /**
     * @brief getEyeCareDate
     * @param temperature
     * @param interpolateStart
     * @param scheduleFrom
     * @param scheduleTo
     */
    void getEyeCareDate(int& temperature, int& interpolateStart, double& scheduleFrom, double& scheduleTo);

    bool isFracDayBetween(double value, double start, double end);
    /**
     * @brief getFracTimeFromDt
     * @param dt
     * @return
     */
    double getFracTimeFromDt(QTime dt);

    /**
     * @brief syncColorSetToKwinWithSetSingleProp
     */
    void syncColorSetToKwinWithSetSingleProp();

    /**
     * @brief syncColorToKwinrc
     */
    void syncColorToKwinrc(QSettings &kwinrcConfig);

    /**
     * @brief syncEyeCareToKwinrc
     */
    void syncEyeCareToKwinrc(QSettings &kwinrcConfig);

    /**
     * @brief checkEyeCareMode
     * @param key
     */
    void checkEyeCareMode(QString key);
    /**
     * @brief isDarkMode
     * @param key
     * @return
     */
    bool isDarkMode(QString key);

    /**
     * @brief sendConfigChangedSignal
     * @param list
     */
    void sendConfigChangedSignal(QStringList list);

    /**
     * @brief disableKwinrcNightColorWithOutConfig
     */
    void disableNightColorWithOutSignal();

    /**
     * @brief checkAutoTheme
     */
    void checkAutoTheme();
private:
    QGSettings *m_pColorSettings;
    QGSettings *m_pQtSettings;
    QGSettings *m_pGtkSettings;
    GmLocation *m_pGmLocation;
    QTimer *m_pTimer;
    UkuiGtkConfig *m_pukuiGtkConfig;
    USD::ClockSkewNotifier *m_pclockSkewNotifier;

    bool m_darkModeChangedBySelf;
    static GammaManagerWayland *m_gammaWaylandManager;

};

#endif // GAMMAMANAGERWAYLAND_H
