#ifndef POWERMANAGERBRIGHTNESS_H
#define POWERMANAGERBRIGHTNESS_H

#include <QVariant>
#include <QDBusMessage>
#include <QDBusInterface>

#include "usd_global_define.h"
#include "QGSettings/qgsettings.h"
#include "abstract-brightness.h"

class PowerManagerBrightness : public AbstractBrightness
{
    Q_OBJECT
public:
    PowerManagerBrightness(QObject *parent);

    /**
     * @brief getBrightness
     * @return
     */
    int getBrightness();

    /**
     * @brief setBrightness
     * @param brightness
     * @return
     */
    int setBrightness(int brightness);

    /**
     * @brief connectTheSignal
     * @return
     */
    int connectTheSignal();

    /**
     * @brief isEnable
     * @return
     */
    QString backend();

private Q_SLOTS:
    void doChanged(QString);

private:
    QGSettings *m_pPowerManger;
};

#endif // POWERMANAGERBRIGHTNESS_H
