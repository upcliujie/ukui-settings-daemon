#include "media-key-cancel.h"
#include <QDBusConnection>
#include <QDBusReply>

#define KGLOBALACCEL_SCHEMA "org.kde.kglobalaccel"
#define KGLOBALACCEL_PATH   "/kglobalaccel"
#define KGLOBALACCEL_INTERFACE "org.kde.KGlobalAccel"

MediaKeyCancel::MediaKeyCancel(QObject *parent) : QObject(parent)
{
    if (!m_kglobalaccel) {
        m_kglobalaccel = new QDBusInterface(KGLOBALACCEL_SCHEMA, KGLOBALACCEL_PATH,
                                            KGLOBALACCEL_INTERFACE, QDBusConnection::sessionBus(), this);
    }
}

const QString MediaKeyCancel::getComponentPath()
{
    if (m_kglobalaccel->isValid()) {
        QDBusReply<QDBusObjectPath> replay = m_kglobalaccel->call("getComponent", m_component);
        if (replay.isValid()) {
            return replay.value().path();
        }
    }
    return QString();
}

void MediaKeyCancel::unRegisterShortcut(const QString &actionId)
{
    if (m_kglobalaccel->isValid()) {
        m_kglobalaccel->call("unregister", m_component, actionId);
    }
}

void MediaKeyCancel::unRegisterAll(const QString& component)
{
    m_component = component;
    const QStringList& actionIds = getActionIds();
    for (const QString &actionId : actionIds) {
        unRegisterShortcut(actionId);
    }
}

const QStringList MediaKeyCancel::getActionIds()
{
    const QString& componetPath = getComponentPath();
    if (!m_component.isEmpty() && !componetPath.isEmpty()) {
        QDBusInterface compoent(KGLOBALACCEL_SCHEMA, getComponentPath(), "org.kde.kglobalaccel.Component", QDBusConnection::sessionBus());
        if (compoent.isValid()) {
            QDBusReply<QStringList> actions = compoent.call("shortcutNames");
            if (actions.isValid()) {
                return actions.value();
            }
        }
    }
    return QStringList();
}
