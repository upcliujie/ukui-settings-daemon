/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef POPWINDOWHELPER_H
#define POPWINDOWHELPER_H

#include <QObject>
#include "devicewindow.h"
#include "volumewindow.h"

class PopWindowHelper : public QObject
{
    Q_OBJECT
public:
    static PopWindowHelper* self();
    explicit PopWindowHelper(QObject *parent = nullptr);
    ~PopWindowHelper();
    Q_INVOKABLE void showWidget(const QString& icon) const;
    Q_INVOKABLE void showWidget(uint value) const;
    Q_INVOKABLE void showWidget(uint value, bool mute) const;
    int getMaxVolume();

    void initWindow();
    void freeWindow();
private:
    DeviceWindow* m_popWindow = nullptr;
    VolumeWindow* m_sliderWindow = nullptr;
};

#endif // POPWINDOWHELPER_H
