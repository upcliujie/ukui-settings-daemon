#ifndef BRIGHTNESSFACTORY_H
#define BRIGHTNESSFACTORY_H

#include <QObject>

class AbstractBrightness : public QObject
{
public:
    AbstractBrightness(){}

    virtual int getBrightness() = 0;

    virtual int setBrightness(int brightness) = 0;

    virtual int connectTheSignal() = 0;

    virtual QString backend() = 0;
};

#endif // BRIGHTNESSFACTORY_H
