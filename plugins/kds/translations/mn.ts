<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn" sourcelanguage="ab">
<context>
    <name>Widget</name>
    <message>
        <location filename="widget.ui" line="14"/>
        <source>KDS</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="widget.ui" line="89"/>
        <source>System Screen Projection</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="widget.ui" line="120"/>
        <source>FirstOutput:</source>
        <translation>ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠳᠡᠯᠭᠡᠴᠡ:</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="141"/>
        <source>None</source>
        <translation>ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="167"/>
        <source>Clone Screen</source>
        <translation>ᠳᠤᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="168"/>
        <source>Extend Screen</source>
        <translation>ᠦᠷᠬᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="263"/>
        <source>Network display</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠦᠵᠡᠬᠦᠷ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
</context>
</TS>
